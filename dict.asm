%include "lib.inc"
global find_word

find_word:
    .loop:
    	
		cmp rsi, 0
		jz .exit
		push rsi
		push rdi
		add rsi, 8
		call string_equals
		pop rdi
		pop rsi
		cmp rax, 1
		jz .result
		mov rsi, [rsi]
		jmp .loop
    .result:
        mov rax, rsi 
        ret
    .exit:
        xor rax, rax
        ret	
