%include "lib.inc"
%include "words.inc"
%define BUFFER_SIZE 0xFF


extern find_word

section .bss
    buffer: resb BUFFER_SIZE 

section .rodata
	mes_too_long: db 'Error: message too long', 0
	mes_not_found: db 'Error: key not found', 0

 
section .text
global _start:
    mov rdi, buffer
    mov rsi, 256
    call read_word
    test rax, rax
    jz .too_long
    mov rdi, rax
    mov rsi, NEXT
    call find_word
    cmp rax, 0
    jz .not_found
    mov rdi, rax
	call print_string
	call print_newline
    xor rdi, rdi
	call exit

	.too_long:
		mov rdi, mes_too_long
        mov rsi, 2
		call print_string
        jmp .end

	.not_found:
		mov rdi, mes_not_found
        mov rsi, 2
		call print_string

    .end:
        add rsp, 256 
        call exit
